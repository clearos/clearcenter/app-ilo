<?php

/**
 * iLO server information view.
 *
 * @category   apps
 * @package    ilo
 * @subpackage views
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/support/documentation/clearos/ilo
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('redfish');

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

echo form_open('ilo/server_info');
echo form_header(lang('redfish_server_information'));

echo field_input('manufacturer', $info['Manufacturer'], lang('redfish_manufacturer'));
echo field_input('model', $info['Model'], lang('redfish_model'));
echo field_input('serial_number', $info['SerialNumber'], lang('redfish_serial_number'));
echo field_input('product_id', $info['SKU'], lang('redfish_product_id'));
echo field_input('uuid', $info['UUID'], lang('redfish_uuid'));
echo field_input('bios_firmware_version', $info['BiosVersion'], lang('redfish_bios_firmware_version'));
echo field_input('intelligent_provisioning_firmware_version', $info['IntelligentProvisioningVersion'], lang('redfish_intelligent_provisioning_firmware_version'));
echo field_input('uid_light_indicator', $info['IndicatorLED'], lang('redfish_uid_light_indicator'));



echo form_footer();
echo form_close();
