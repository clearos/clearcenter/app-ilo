<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'ilo';
$app['version'] = '1.0.18';
$app['release'] = '1';
$app['vendor'] = 'ClearCenter';
$app['packager'] = 'ClearCenter';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('ilo_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('ilo_app_name');
$app['category'] = lang('base_category_server');
$app['subcategory'] = lang('base_subcategory_management');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_requires'] = array(
    'app-redfish-core >= 1:1.0.15',
    'clearos-framework >= 7.5.25',
);

$app['core_directory_manifest'] = array(
    '/var/clearos/ilo' => array(),
    '/var/clearos/ilo/backup' => array(),
);
