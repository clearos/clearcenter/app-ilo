<?php

/**
 * iLO bios view.
 *
 * @category   apps
 * @package    ilo
 * @subpackage views
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/support/documentation/clearos/ilo
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('redfish');

$read_only = TRUE;


///////////////////////////////////////////////////////////////////////////////
// Boot Order
///////////////////////////////////////////////////////////////////////////////

$anchors = [];
$headers = [
    lang('redfish_boot_option_number'),
    lang('redfish_boot_string'),
    lang('redfish_structured_boot_string')
];
$options['no_action'] = TRUE;

foreach ($boot_list['BootSources'] as $id => $source) {
   

    $item['details'] = array(

        $source->BootOptionNumber,
        $source->BootString,
        $source->StructuredBootString
    );

    $items[] = $item;
}

echo summary_table(
    lang('redfish_boot_order'),
    $anchors,
    $headers,
    $items,
    $options
);

