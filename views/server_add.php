<?php

/**
 * iLO server information view.
 *
 * @category   apps
 * @package    ilo
 * @subpackage views
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/support/documentation/clearos/ilo
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('redfish');

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

$buttons = array( 
        form_submit_add('submit'),
        anchor_cancel('/app/ilo')
);

echo form_open('ilo/add');
echo form_header(lang('redfish_server_information'));

echo field_input('profile', NULL, lang('redfish_profile'));
echo field_input('address', NULL, lang('redfish_address'));
echo field_input('description', NULL, lang('redfish_description'));
echo field_input('username', NULL, lang('redfish_username'));
echo field_input('password', NULL, lang('redfish_password'));
echo field_button_set($buttons);
echo form_footer();
echo form_close();