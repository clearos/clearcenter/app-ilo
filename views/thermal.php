<?php

/**
 * iLO thermal view.
 *
 * @category   apps
 * @package    ilo
 * @subpackage views
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/support/documentation/clearos/ilo
 */



///////////////////////////////////////////////////////////////////////////////
// Fans
///////////////////////////////////////////////////////////////////////////////

$anchors = [];
$headers = [
    lang('redfish_fan_name'),
    lang('redfish_fan_reading'),
    lang('base_status'),
];
$options['no_action'] = TRUE;

foreach ($info['Fans'] as $id => $fan) {
   
    $status = $fan->Status->State;

    if($fan->Status->State == 'Enabled')
        $status = $status .'/'.$fan->Status->Health; 

    $item['details'] = array(
        $fan->Name,
        $fan->Reading.' '.$fan->ReadingUnits,
        $status
    );
    

    $items[] = $item;
}

echo summary_table(
    lang('redfish_fans'),
    $anchors,
    $headers,
    $items,
    $options
);


///////////////////////////////////////////////////////////////////////////////
// Temperatures
///////////////////////////////////////////////////////////////////////////////

$items = [];
$options = [];
$anchors = [];
$headers = [
    lang('redfish_temperature_sensor_number'),
    lang('redfish_temperature_name'),
    lang('redfish_temperature_physical_context'),
    lang('redfish_temperature_reading_celsius'),
    lang('redfish_temperature_upper_thresholds'),
    lang('base_status'),
];
$options['no_action'] = TRUE;

foreach ($info['Temperatures'] as $id => $temperature) {
   
    $status = $temperature->Status->State;

    if($temperature->Status->State == 'Enabled')
        $status = $status .'/'.$temperature->Status->Health; 

    $item['details'] = array(
        $temperature->SensorNumber,
        $temperature->Name,
        $temperature->PhysicalContext,
        $temperature->ReadingCelsius,
        $temperature->UpperThresholdCritical .'/'. $temperature->UpperThresholdFatal,
        $status
    );
    
    $items[] = $item;
}

echo summary_table(
    lang('redfish_temperatures'),
    $anchors,
    $headers,
    $items,
    $options
);

