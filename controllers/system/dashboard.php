<?php

/**
 * iLO dashboard controller.
 *
 * @category   apps
 * @package    ilo
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/docs/developer/apps/ilo
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * iLO dashboard controller.
 *
 * @category   apps
 * @package    ilo
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/docs/developer/apps/ilo
 */

class Dashboard extends ClearOS_Controller
{
    /**
     * iLO dashboard summary view.
     *
     * @return view
     */

    function index($server, $id)
    {
        // Load libraries
        //---------------

        $this->lang->load('ilo');
        $this->load->library('redfish/System_Library', $server);


        // Lets make a try if the server has some access issues so we can avoid loading times with multiple controller

        try {
            $this->system_library->get_info($id);
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }


        // If pass Load views
        //-----------

        $controllers = array(
            array(
                'controller' => 'ilo/system/info',
                'method' => 'index',
                'params' => [ $server, $id ]
            ),
            array(
                'controller' => 'ilo/system/power',
                'method' => 'index',
                'params' => [ $server, $id ]
            ),
            array(
                'controller' => 'ilo/system/bios',
                'method' => 'index',
                'params' => [ $server, $id ]
            ),
            /*array(
                'controller' => 'ilo/system/chassis',
                'method' => 'index',
                'params' => [ $server, $id ]
            ),*/
            array(
                'controller' => 'ilo/system/thermal',
                'method' => 'index',
                'params' => [ $server, $id ]
            ),
            array(
                'controller' => 'ilo/system/ethernet_interfaces',
                'method' => 'index',
                'params' => [ $server, $id ]
            ),
            array(
                'controller' => 'ilo/system/smart_storage',
                'method' => 'index',
                'params' => [ $server, $id ]
            ),
        );

        $this->page->view_controllers($controllers, lang('ilo_app_name'));
    }
}
